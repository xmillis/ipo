package com.xmillis.ipo;

import java.util.ArrayList;
import java.util.Collection;

/**
 * The output of a process
 * 
 * @author Teo
 *
 * @param <T> The type of the wrapped output value
 */
public class Output<T> extends Input<T> {

	/**
	 * Construct the output of a process
	 * 
	 * @param value Actual, wrapped output value
	 */
	public Output(T value) {
		super(value);
	}
	
	/**
	 * Helper factory method
	 * 
	 * @param value Value to be wrapped as an output
	 * @return an input wrapping the given value
	 */
	public static <T> Output<T> from(T value) {
		return new Output<T>(value);
	}
	
	/**
	 * Helper factory method
	 * 
	 * @param values Values to be wrapped as an output
	 * @return an output wrapping the given values
	 */
	@SafeVarargs
	public static <T> Output<Collection<T>> from(T... values) {
		final Collection<T> outputs = new ArrayList<T>();
		for (int i = 0; i < values.length; i++) {
			outputs.add(values[i]);
		}
		return new Output<Collection<T>>(outputs);
	}
}
