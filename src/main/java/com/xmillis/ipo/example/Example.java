package com.xmillis.ipo.example;

import com.xmillis.ipo.Input;
import com.xmillis.ipo.Output;
import com.xmillis.ipo.common.Filter;
import com.xmillis.ipo.performance.MonitoredInput;
import com.xmillis.ipo.Process;

/**
 * Some examples of what the library can do
 * 
 * @author Teo
 */
public class Example {
	
	public static void main(String[] args) {
		findMeSomePalindromes();
	}

	private static void findMeSomePalindromes() {
		
		Filter<String> palindromeFilter = new Filter<String>() {
			@Override
			protected boolean accept(String string) {
				for (int i = 0; i < string.length() / 2; i++) {
					if (string.charAt(i) != string.charAt(string.length() - i - 1)) {
						return false;
					}
				}
				return true;
			}
		};
		
		// One way to do it
		String[] array = { "bla", "aha", "yes", "apapa" };
		Input.from(array).to(palindromeFilter).to(Process.PRINT);

		// Another way to do it
		Input.from("bla", "aha", "ahaha").to(palindromeFilter, Process.PRINT);
	}

	@SuppressWarnings("unused")
	private static void makeAnHttpRequest() {
		Input.from("http://xmillis.com").to(Process.REQUEST, Process.PRINT);
	}
	
	@SuppressWarnings("unused")
	private static void printSomeSquareRoots() {
		Process<Double, Double> sqrt = new Process<Double, Double>() {
			public Output<Double> process(Double input) {
				return new Output<Double>(Math.sqrt(input));
			}
		};
		Process.PRINT.process(sqrt.process(Input.from(81.0)));
	}
	
	@SuppressWarnings("unused")
	private static void somePerformanceMonitoring() {
		Input<String> url = new MonitoredInput<String>("https://currentmillis.com/time/minutes-since-unix-epoch.php") {
			@Override
			protected void onMonitoredEnd(Process<?, ?> process, boolean isFirstProcess, boolean isLastProcess, long inputCreationTime, long processStartTime, long outputCreationTime) {
				System.out.println(process + " finished after " + (outputCreationTime - processStartTime) + "ms");
			}
		};
		url.to(Process.REQUEST, Process.PRINT);	
	}
}
