package com.xmillis.ipo;

import com.xmillis.ipo.common.Print;
import com.xmillis.ipo.common.Request;

/**
 * A series of computations/actions/events that produces output based on some input
 * 
 * @author Teo
 *
 * @param <I> Type of input to be processed
 * @param <O> Type of output to be produced
 */
public abstract class Process<I,O> {
	
	/** Process that prints to STDOUT */
	public static final Process<Object, Object> PRINT = new Print<Object>();
	
	/** Process that makes an HTTP request */
	public static final Process<String, String> REQUEST = new Request();
	
	/**
	 * Process the given input
	 * 
	 * @param input A wrapped input value
	 * @return A wrapped output
	 * @throws ProcessException
	 */
	public final Output<O> process(Input<? extends I> input) throws ProcessException {
		return input.to(this);
	}
	
	/**
	 * Process the given input
	 * 
	 * @param inputValue An unwrapped input value
	 * @return A wrapped output
	 * @throws ProcessException
	 */
	protected abstract Output<O> process(I inputValue) throws ProcessException;
	
	/**
	 * Concatenates this process with the given process.
	 * 
	 * @param second
	 * @return A new process with input of the same type as this process and output of the same type as the given process
	 */
	public final <O2> Process<I,O2> and(Process<O,O2> second) {
		final Process<I,O> first = this;
		return new Process<I,O2>() {
			@Override
			protected Output<O2> process(I inputValue) throws ProcessException {
				return first.process(inputValue).to(second);
			}
		};
	}
	
	/**
	 * @return The name of this process
	 */
	public String getName() {
		return this.toString();
	}
	
	/**
	 * Runtime exception which will be thrown by the IPO library
	 * 
	 * @author Teo
	 */
	public static class ProcessException extends RuntimeException {
		private static final long serialVersionUID = 8198189460991466137L;

		public ProcessException(Throwable throwable) {
			super(throwable);
		}
	}
}
