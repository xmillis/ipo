package com.xmillis.ipo.common.entities;

import com.xmillis.ipo.Output;
import com.xmillis.ipo.Process;

/**
 * A process which may handle or store the input but may not alter it on its way
 * out. The wrapped output value will reference exactly the same object as the
 * wrapped input value
 * 
 * @author Teo
 *
 * @param <T>
 */
public abstract class Destination<T> extends Process<T, T> {

	@Override
	protected final Output<T> process(T inputValue) throws ProcessException {
		onProcess(inputValue);
		return Output.from(inputValue);
	}

	/**
	 * Handling, storing, display operations
	 */
	protected abstract void onProcess(T inputValue) throws ProcessException;
}
