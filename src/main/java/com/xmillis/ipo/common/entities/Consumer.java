package com.xmillis.ipo.common.entities;

import com.xmillis.ipo.Process;
import com.xmillis.ipo.common.math.Void;

/**
 * A process which may handle or store the input but will not return it as an output
 * 
 * @author Teo
 *
 * @param <T>
 */
public abstract class Consumer<T> extends Process<T, T> {

	@Override
	protected final Void<T> process(T inputValue) throws ProcessException {
		onProcess(inputValue);
		return new Void<T>();
	}
	
	/**
	 * Handling, storing, display operations
	 */
	protected abstract void onProcess(T inputValue) throws ProcessException;
}
