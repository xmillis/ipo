package com.xmillis.ipo.common;
import com.xmillis.ipo.common.entities.Destination;

/**
 * Process that prints the string representation of the object to STDOUT.
 * Wrapped value of the output is identical to that of the input.
 * 
 * @author Teo
 */
public final class Print<T> extends Destination<T> {

	@Override
	protected void onProcess(T inputValue) {
		System.out.println(inputValue);
	}
}
