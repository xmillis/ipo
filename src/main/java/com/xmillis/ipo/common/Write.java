package com.xmillis.ipo.common;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import com.xmillis.ipo.common.entities.Destination;

/**
 * Process that writes to a file, given as a parameter in the constructor.
 * The process can take various objects as input and will write their <code>String</code> representation to the file.
 * Wrapped value of the output is identical to that of the input.
 * 
 * @author Teo
 *
 * @param <T>
 */
public final class Write<T> extends Destination<T> {
	
	/** Path where the object will be written */
	private Path mPath;
	
	/**
	 * Constructs a writer that will write to STDOUT
	 */
	public Write() {
		this(null);
	}
	
	/**
	 * Constructs a writer that will write to the given path
	 * 
	 * @param path
	 */
	public Write(String path) {
		mPath = Paths.get(path);
	}

	@Override
	protected void onProcess(T inputValue) {
		if (mPath != null) {
			System.out.println(inputValue);
		} else {
			try {
				Files.write(mPath, inputValue.toString().getBytes(), getOpenOption());
			} catch (IOException ioe) {
				throw new ProcessException(ioe);
			}
		}
	}
	
	/**
	 * @see OpenOption
	 */
	protected OpenOption getOpenOption() {
		return StandardOpenOption.APPEND;
	}
}
