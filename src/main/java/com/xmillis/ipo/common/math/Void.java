package com.xmillis.ipo.common.math;

import com.xmillis.ipo.Output;

/**
 * Emptiness. Null output value.
 * 
 * @author Teo
 *
 * @param <T>
 */
public class Void<T> extends Output<T> {

	public Void() {
		super(null);
	}
}
