package com.xmillis.ipo.common.math;

import com.xmillis.ipo.common.entities.Destination;

/**
 * A destination which leaves the input untouched, not even read or copied.
 * 
 * @author Teo
 *
 * @param <T>
 */
public final class Identity<T> extends Destination<T> {
	
	@Override
	protected void onProcess(T inputValue) {
		// do nothing for a perfect identity
	}
	
}
