package com.xmillis.ipo.common;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.xmillis.ipo.Output;
import com.xmillis.ipo.Process;

/**
 * Filter process implementing a generic filter pattern
 * 
 * @author Teo
 *
 * @param <T> Type of the objects being filtered
 */
public abstract class Filter<T> extends Process<Iterable<T>, Iterable<T>> {

	@Override
	protected final Output<Iterable<T>> process(Iterable<T> input) {
		final Collection<T> outputs = new ArrayList<T>();
		final Iterator<T> iterator = input.iterator();
		while (iterator.hasNext()) {
			T object = iterator.next();
			if (accept(object)) {
				outputs.add(object);
			}
		}
		return new Output<Iterable<T>>(outputs);
	}

	/**
	 * Which objects will transported in the output?
	 * 
	 * @param object The object under question
	 * @return true if the object is to be part of the output of this filter, false otherwise
	 */
	protected abstract boolean accept(T object);
}
