package com.xmillis.ipo.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.xmillis.ipo.Output;
import com.xmillis.ipo.Process;

/**
 * Process that makes an HTTP request.
 * Takes a <code>String</code> URL as an input and produces the response as a <code>String</code> 
 * 
 * @author Teo
 */
public final class Request extends Process<String, String> {

	@Override
	protected Output<String> process(String url) {
		try {
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			
			in.close();
			return new Output<String>(response.toString());
		} catch (IOException ioe) {
			throw new ProcessException(ioe);
		}
	}

}
