package com.xmillis.ipo.performance;

import com.xmillis.ipo.Process;

abstract class Monitor<X> extends Process<X,X> {
	
	@Override
	protected final MonitoredInput<X> process(X inputValue) throws ProcessException {
		return new MonitoredInput<X>(inputValue) {
			@Override
			protected void onMonitoredEnd(Process<?, ?> process, boolean isFirstProcess, boolean isLastProcess,
					long inputCreationTime, long processStartTime, long outputCreationTime) {
				carryOnMonitoredEnd(process, isLastProcess, inputCreationTime, processStartTime, outputCreationTime);
			}
		};
	}
	
	protected abstract void carryOnMonitoredEnd(Process<?,?> process, boolean isLastProcess, long inputCreationTime, long processStartTime, long outputCreationTime);
}