package com.xmillis.ipo.performance;

import com.xmillis.ipo.Output;
import com.xmillis.ipo.Process;

/**
 * An input which propagates a monitored callback through the process chain
 * 
 * @author Teo
 *
 * @param <T> The type of the wrapped input value
 */
public abstract class MonitoredInput<T> extends Output<T> {

	/** Unix timestamp in milliseconds of the moment this object was created */
	private final long mCreationTime;
	
	/** The callback propagates inside a chain, but will it propagate to subsequent chains? */
	private final boolean mIsPropagatingCallbackAcrossChains;

	/**
	 * Constructs a monitored input
	 * 
	 * @param value
	 *            Wrapped input value
	 */
	public MonitoredInput(T value) {
		this(value, false);
	}
	
	/**
	 * Constructs a monitored input
	 * 
	 * @param value
	 *            Wrapped input value
	 * @param isPropagatingCallbackAcrossChains
	 *            If true the callback will be called not only in subsequent
	 *            processes of the same chain, but in subsequent chains as well. Use with caution.
	 *            By default this is false.
	 */
	public MonitoredInput(T value, boolean isPropagatingCallbackAcrossChains) {
		super(value);
		mCreationTime = System.currentTimeMillis();
		mIsPropagatingCallbackAcrossChains = isPropagatingCallbackAcrossChains;
	}

	@Override
	public <O> Output<O> to(Process<? super T, O> process) throws ProcessException {
		long processStartTime = System.currentTimeMillis();
		Output<O> output = super.to(process);
		if (mIsPropagatingCallbackAcrossChains) {
			output = (MonitoredInput<O>) output.to(new Monitor<O>() {
				@Override
				protected void carryOnMonitoredEnd(Process<?, ?> process, boolean isLastProcess, long inputCreationTime,
						long processStartTime, long outputCreationTime) {
					onMonitoredEnd(process, false, isLastProcess, inputCreationTime, processStartTime, outputCreationTime);
				}
			});
		}
		onMonitoredEnd(process, true, true, mCreationTime, processStartTime, System.currentTimeMillis());
		return output;
	}
	
	@Override
	public <O1, O2> Output<O2> to(Process<? super T, O1> process1, Process<? super O1, O2> process2)
			throws ProcessException {
		long processStartTime = System.currentTimeMillis();
		Output<O1> output1 = super.to(process1);
		long newProcessStartTime = System.currentTimeMillis();
		onMonitoredEnd(process1, true, false, mCreationTime, processStartTime, newProcessStartTime);
		Output<O2> output2 = output1.to(process2);
		if (mIsPropagatingCallbackAcrossChains) {
			output2 = (MonitoredInput<O2>) output2.to(new Monitor<O2>() {
				@Override
				protected void carryOnMonitoredEnd(Process<?, ?> process, boolean isLastProcess, long inputCreationTime,
						long processStartTime, long outputCreationTime) {
					onMonitoredEnd(process, false, isLastProcess, inputCreationTime, processStartTime, outputCreationTime);
				}
			});
		}
		onMonitoredEnd(process2, false, true, mCreationTime, newProcessStartTime, System.currentTimeMillis());
		return output2;
	}

	/**
	 * Callback to be overriden by monitored input users
	 * This callback will be called after each process in the process chain that this precise input is passed to.
	 * @see {@link #to(Process)}, {@link #to(Process, Process)}
	 * 
	 * @param process The process which is about to process this input or a descendant of this input
	 * @param isFirstProcess
	 * @param isLastProcess
	 * @param inputCreationTime
	 * @param processStartTime
	 * @param outputCreationTime
	 */
	protected abstract void onMonitoredEnd(Process<?, ?> process, boolean isFirstProcess, boolean isLastProcess,
			long inputCreationTime, long processStartTime, long outputCreationTime);
}