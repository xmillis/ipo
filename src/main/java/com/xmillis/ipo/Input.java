package com.xmillis.ipo;

import java.util.ArrayList;
import java.util.Collection;

import com.xmillis.ipo.common.math.Void;

/**
 * The input of a process
 * 
 * @author Teo
 *
 * @param <T> The type of the wrapped input value
 */
public class Input<T> extends Process<T,T> {
	
	/** Wrapped input value */
	private T mValue;

	/**
	 * Construct the input of a process
	 * 
	 * @param value The wrapped value
	 */
	public Input(T value) {
		mValue = value;
	}
	
	/**
	 * @return Wrapped value
	 */
	protected final T getValue() {
		return mValue;
	}
	
	/**
	 * Send this input to the given process
	 * 
	 * @param process The process that will process this input
	 * @return An <code>Output</code> encapsulating the result
	 * @throws ProcessException
	 */
	public <O> Output<O> to(Process<? super T,O> process) throws ProcessException {
		Output<O> output = null;
		try {
			onStart(process, true, mValue);
			output = process.process(mValue);
			onEnd(process, true, mValue, output);
		} catch (ProcessException pe) {
			onThrow(process, pe);
			throw pe;
		}
		return output;
	}
	
	/**
	 * Send this input to the given processes, in order
	 * 
	 * @param process1 The 1st process that will process this input
	 * @param process2 The 2nd process (will process the output from the 1st process)
	 * @return An <code>Output</code> encapsulating the result. This will actually be the <code>Output</code> of the second process
	 * @throws ProcessException
	 */
	public <O1,O2> Output<O2> to(Process<? super T,O1> process1, Process<? super O1, O2> process2) throws ProcessException {
		Output<O1> output = null;
		try {
			onStart(process1, true, mValue);
			output = process1.process(mValue);
			onEnd(process1, false, mValue, output.getValue());
		} catch (ProcessException pe) {
			onThrow(process1, pe);
			throw pe;
		}
		Output<O2> output2 = null;
		try {
			onStart(process2, false, output.getValue());
			output2 = process2.process(output.getValue());
			onEnd(process2, true, output.getValue(), output2.getValue());
		} catch (ProcessException pe) {
			onThrow(process2, pe);
			throw pe;
		}
		return output2;
	}
	
	/**
	 * Callback on process start, before the actual processing starts.
	 * This callback will be called before each process in the process chain that this precise input is passed to.
	 * @see {@link #to(Process)}, {@link #to(Process, Process)}
	 * 
	 * @param process The process which is about to process this input or a descendant of this input
	 * @param isFirstProcess Is this the entry-level process?
	 * @param inputValue Input value as it will be processed
	 */
	protected void onStart(Process<?,?> process, boolean isFirstProcess, Object inputValue) {
		// overrideable callback
	}
	
	/**
	 * Callback at the end of the process, after the actual processing happened
	 * This callback will be called after each process in the process chain that this precise input is passed to.
	 * @see {@link #to(Process)}, {@link #to(Process, Process)}
	 * 
	 * @param process The process which is about to process this input or a descendant of this input
	 * @param isLastProcess Is this the exit-level process?
	 * @param inputValue Input value as it has been processed
	 * @param outputValue The actual result of processing the input value
	 */
	protected void onEnd(Process<?,?> process, boolean isLastProcess, Object inputValue, Object outputValue) {
		// overrideable callback
	}
	
	/**
	 * Callback after a throwable is thrown
	 * This callback will be called in case any process in the process chain will throw a throwable
	 * @see {@link #to(Process)}, {@link #to(Process, Process)}
	 * 
	 * @param process The process which is about to process this input or a descendant of this input
	 * @param throwable The throwable which was thrown
	 */
	protected void onThrow(Process<?,?> process, Throwable throwable) {
		// overrideable callback
	}
	
	/**
	 * Assert that the value wrapped by this input is equal to the given expected value
	 * 
	 * @param expected
	 */
	public void assertEquals(T expected) {
		if ((expected == null && getValue() != null)  || (expected != null && getValue() == null) ||
				(expected != null && getValue() != null && !expected.equals(getValue()))) {
			throw new AssertionError("Expected " + expected + " but was " + getValue());
		}
	}

	/**
	 * Helper factory method
	 * 
	 * @param value Value to be wrapped as an input
	 * @return an input wrapping the given value
	 */
	public static <T> Input<T> from(T value) {
		return new Input<T>(value);
	}

	/**
	 * Helper factory method
	 * 
	 * @param values Values to be wrapped as an input
	 * @return an input wrapping the given values
	 */
	@SafeVarargs
	public static <T> Input<Collection<T>> from(T... values) {
		final Collection<T> inputs = new ArrayList<T>();
		for (int i = 0; i < values.length; i++) {
			inputs.add(values[i]);
		}
		return new Input<Collection<T>>(inputs);
	}

	@Override
	protected final Output<T> process(T input) {
		return new Void<T>();
	}
}
