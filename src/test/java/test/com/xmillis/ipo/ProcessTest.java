package test.com.xmillis.ipo;

import org.junit.Test;

import com.xmillis.ipo.Input;
import com.xmillis.ipo.Output;
import com.xmillis.ipo.Process;

public class ProcessTest {

	@Test
	public void testProcess() {
		
		Process<Double, Double> sqrt = new Process<Double, Double>() {
			public Output<Double> process(Double input) {
				return new Output<Double>(Math.sqrt(input));
			}
		};
		
		Input.from(64.0).to(sqrt).assertEquals(8.0);
		Input.from(16.0).to(sqrt,sqrt).assertEquals(2.0);
		
		Process<Double, Double> aFourth = sqrt.and(sqrt);
		Input.from(16.0).to(aFourth).assertEquals(2.0);
	}

}
