package test.com.xmillis.ipo;

import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Assert;
import org.junit.Test;

import com.xmillis.ipo.Input;
import com.xmillis.ipo.Output;
import com.xmillis.ipo.Process;
import com.xmillis.ipo.Process.ProcessException;

public class InputTest {

	@Test
	public void testOnProcessStart() {
		final AtomicBoolean state = new AtomicBoolean(false);
		Input<Double> number = new Input<Double>(256.0) {
			@Override
			protected void onStart(Process<?, ?> process, boolean isFirstProcess, Object inputValue) {
				System.out.println(inputValue.toString() + " about to be processed by " + process.getName());
				if (!state.get()) {
					Assert.assertTrue(isFirstProcess);
					Assert.assertEquals(Double.valueOf(256), inputValue);
					state.set(true);
				} else {
					Assert.assertFalse(isFirstProcess);
					Assert.assertEquals(Double.valueOf(16), inputValue);
					state.set(false);
				}
			}
		};
		Process<Double, Double> sqrt = new Process<Double, Double>() {
			@Override
			public Output<Double> process(Double input) {
				System.out.println(getName() + " processing " + input);
				Assert.assertEquals(input == 256.0, state.get());
				return new Output<Double>(Math.sqrt(input));
			}
		};
		number.to(sqrt, sqrt).assertEquals(4.0);
	}
	
	@Test
	public void testOnProcessEnd() {
		final AtomicBoolean state = new AtomicBoolean(false);
		Input<Double> number = new Input<Double>(256.0) {
			@Override
			protected void onEnd(Process<?, ?> process, boolean isLastProcess, Object inputValue, Object outputValue) {
				System.out.println(inputValue.toString() + " was processed by " + process.getName());
				if (!state.get()) {
					Assert.assertFalse(isLastProcess);
					Assert.assertEquals(Double.valueOf(256), inputValue);
					state.set(true);
				} else {
					Assert.assertTrue(isLastProcess);
					Assert.assertEquals(Double.valueOf(16), inputValue);
					state.set(false);
				}
			}
		};
		Process<Double, Double> sqrt = new Process<Double, Double>() {
			@Override
			public Output<Double> process(Double input) {
				System.out.println(getName() + " processing " + input);
				Assert.assertEquals(input == 16.0, state.get());
				return new Output<Double>(Math.sqrt(input));
			}
		};
		number.to(sqrt, sqrt).assertEquals(4.0);
	}

	@Test (expected = ProcessException.class)
	public void testOnProcessThrow() {
		final AtomicBoolean state = new AtomicBoolean(false);
		Input<Double> number = new Input<Double>(256.0) {
			@Override
			protected void onStart(Process<?, ?> process, boolean isFirstProcess, Object inputValue) {
				System.out.println(inputValue.toString() + " about to be processed by " + process.getName());
				if (!state.get()) {
					Assert.assertTrue(isFirstProcess);
					Assert.assertEquals(Double.valueOf(256), inputValue);
					state.set(true);
				} else {
					Assert.assertFalse(isFirstProcess);
					Assert.assertEquals(Double.valueOf(16), inputValue);
					state.set(false);
				}
			}
			@Override
			protected void onThrow(Process<?, ?> process, Throwable throwable) {
				System.out.println(process.getName() + " threw " + throwable.toString());
			}
		};
		Process<Double, Double> sqrt = new Process<Double, Double>() {
			@Override
			public Output<Double> process(Double input) {
				System.out.println(getName() + " processing " + input);
				Assert.assertEquals(input == 256.0, state.get());
				if (input == 16.0) {
					throw new ProcessException(new IllegalAccessException());
				}
				return new Output<Double>(Math.sqrt(input));
			}
		};
		try {
			number.to(sqrt, sqrt).assertEquals(4.0);
		} catch (ProcessException pe) {
			Assert.assertTrue(pe.getCause() instanceof IllegalAccessException);
			throw pe;
		}
	}
}
