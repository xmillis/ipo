package test.com.xmillis.ipo.common.math;

import org.junit.Test;

import com.xmillis.ipo.Input;
import com.xmillis.ipo.Process;
import com.xmillis.ipo.common.math.Identity;

public class IdentityTest {

	@Test
	public void testProcess() {
		Process<Object, Object> identity = new Identity<Object>();
		Input.from("string").to(identity).assertEquals("string");
	}
}
