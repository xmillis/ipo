package test.com.xmillis.ipo.common.math;

import org.junit.Test;

import com.xmillis.ipo.Input;
import com.xmillis.ipo.common.math.Void;

public class VoidTest {

	@Test
	public void testProcess() {
		Input.from("string").to(new Void<Object>()).assertEquals(null);
	}
}
