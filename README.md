# README #

### What is this repository for? ###

This repository hosts an open-sourced IPO library for Java. Please visit https://bitbucket.org/xmillis/ipo/wiki/ for more information.

### How do I get set up? ###
For now, there is a snapshot, if you want to use it just put this in your pom:
```
#!xml
<dependency>
	<groupId>com.xmillis</groupId>
	<artifactId>ipo</artifactId>
	<version>0.1-SNAPSHOT</version>
</dependency>
```
### Contribution guidelines ###
...coming soon...

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
* Repo owner: Teo Filimon
* Contact me at: email@teofilimon.com